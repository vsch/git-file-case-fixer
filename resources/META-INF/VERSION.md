# Version History

[TOC]: # " "

- [Next: 1.0.6](#next-106)
- [1.0.4](#104)
- [1.0.2](#102)
- [1.0.0](#100)
- [0.0.0](#000)

## Next: 1.0.6

* Fix: handle parent directories' case mismatch in apply fixes. Right now fix git will work, but
  fix file system will only work for case mismatch in the file name.
* Fix: files with mismatched case could be doubled in the preview git file case fixer dialog.
* Fix: mark renamed files as vcs dirty so they get refreshed, otherwise they don't make it into
  the next commit.
* Fix: make default settings check unmodified files
* Fix: improve text and link layout of check-in handler.
* Fix: use LinkLabel for check-in handler options

## 1.0.4

* Fix: compatibility with 2016.3.8, some API uses were from later versions
* Fix: no mismatches balloon popup would show in first project instead of the current one.

## 1.0.2

* Fix: show mismatches action would not check files unless `check all files` was set in
  configuration
* Fix: message for modified and unmodified found files.
* Fix: vcs tool window icon

## 1.0.0

* Initial release

## 0.0.0

* first commit

![ScreenShot_CommitDialog.png](../../assets/images/ScreenShot_CommitDialog.png)
