Git File Case Mismatch Plugin
=============================

Detect and fix file case mismatch between git and the file system

Adds **Before Commit** check for file case mismatches between git and the file system
with corrective actions:

* Change git file case to match file system
* Change file system case to match git

![ScreenShot_ShowMismatchesDialog.png](https://github.com/vsch/git-file-case-fixer/raw/master/assets/images/ScreenShot_ShowMismatchesDialog.png)
